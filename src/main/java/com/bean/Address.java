package com.bean;

public class Address {
    private Integer id;

    private Integer userId;

    private Integer countryId;

    private String name;

    private Integer cityId;

    private String address;

    private String company;

    private String tel;

    private String postalPrefix;

    private String postalSuffix;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getPostalPrefix() {
        return postalPrefix;
    }

    public void setPostalPrefix(String postalPrefix) {
        this.postalPrefix = postalPrefix == null ? null : postalPrefix.trim();
    }

    public String getPostalSuffix() {
        return postalSuffix;
    }

    public void setPostalSuffix(String postalSuffix) {
        this.postalSuffix = postalSuffix == null ? null : postalSuffix.trim();
    }
}
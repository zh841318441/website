package com.bean;

public class CityDic {
    private Integer id;

    private Integer countryDicId;

    private String city;

    private String code;

    private Integer mialboxId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryDicId() {
        return countryDicId;
    }

    public void setCountryDicId(Integer countryDicId) {
        this.countryDicId = countryDicId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public Integer getMialboxId() {
        return mialboxId;
    }

    public void setMialboxId(Integer mialboxId) {
        this.mialboxId = mialboxId;
    }
}
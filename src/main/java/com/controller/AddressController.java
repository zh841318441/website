package com.controller;

import com.bean.Address;
import com.bean.CityDic;
import com.bean.CountryDic;
import com.bean.Maps;
import com.service.imp.AddressMessage;
import com.util.BackStatus;
import com.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 9:58 2019/3/7 0007
 */
@Controller
@RequestMapping("/address")
public class AddressController {
    @Autowired
    AddressMessage addressMessage;
    @RequestMapping("/showAddress")
    public String showAddress(Model model) {
        List<CountryDic> mapsList = addressMessage.selectAllCountry();
        model.addAttribute("maps", mapsList);
        return "address";
    }

    @RequestMapping("/selectAllCityByCountry/{id}")
    @ResponseBody
    public Result selectAllCityByCountry(@PathVariable Integer id) {
        List<CityDic> cityDicList = addressMessage.selectAllCityByCountry(id);
        return Result.build(200, "", cityDicList);
    }

    @RequestMapping("/selectCityByMailBox/{mailBox}")
    @ResponseBody
    public Result selectCityByMailBox(@PathVariable  String mailBox) {
        Integer mailBoxId = addressMessage.selectMailBoxId(mailBox);
        return Result.build(200, "", mailBoxId);
    }

    @RequestMapping("/save")
    @ResponseBody
    public Result saveAddress(Address address) {
        String states = addressMessage.saveAddress(address);
        if (BackStatus.SUCCESS.equals(states)) {
            return Result.build(200, "保存成功");
        }
        return Result.build(500, "保存失败");
    }
}

package com.controller;

import com.bean.Card;
import com.dao.CardMapper;
import com.service.imp.CardMessage;
import com.util.BackStatus;
import com.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 21:58 2019/3/7 0007
 */
@Controller
@RequestMapping("/card")
public class CardController {
    @Autowired
    CardMessage cardMessage;
    @RequestMapping("/saveCard")
    @ResponseBody
    public Result saveCard(Card card){
        String states=cardMessage.saveCard(card);
        if(BackStatus.SUCCESS.equals(states)){
            return Result.build(200,"保存成功");
        }
        return Result.build(500,"保存失败");
    }

    @RequestMapping("/showCard/{id}")
    public String showCard(Model model, @PathVariable int id){
        Map map=cardMessage.selectDate();
        model.addAttribute("day",map.get("day"));
        model.addAttribute("year",map.get("year"));
        model.addAttribute("id",id);
        return "change-account";
    }
    @RequestMapping("/showSend")
    public String showSend(){
        return "profiling-receive";
    }
}

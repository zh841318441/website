package com.controller;

import com.bean.CountryDic;
import com.bean.User;
import com.service.LoginService;
import com.service.imp.AddressMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 22:31 2019/3/6 0006
 */
@Controller
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    AddressMessage addressMessage;
    @RequestMapping("/showLoginPage")
    public String showLoginPage() {
        return "/loginj";
    }

    @RequestMapping("login")
    public String login(@RequestParam("email") String email,
                        @RequestParam("password") String password,
                        Model model) {
        model.addAttribute("paramErrorMsg",null);
        model.addAttribute("codeErrorMsg",null);
        model.addAttribute("loginErrorMsg",null);
        if (StringUtils.isBlank(email) || StringUtils.isBlank(password) ) {
            model.addAttribute("paramErrorMsg", "参数错误");
            return "loginj";
        }
        int id = loginService.insertUser(email, password);
        List<CountryDic> mapsList = addressMessage.selectAllCountry();
        model.addAttribute("maps", mapsList);
        model.addAttribute("id",id);
        return "address";
    }

    @RequestMapping("sendCode")
    @ResponseBody
    public String sendCode(@RequestParam("email") String email, HttpSession seesion) {
        if (StringUtils.isBlank(email)) {
            return "param";
        }
        String random = RandomStringUtils.random(4, false, true);
        seesion.setAttribute("code", random);
        seesion.setAttribute("codeTime", System.currentTimeMillis());
        loginService.sendEmail(email, loginService.getSubject(), loginService.getCodeTemplate(random));
        return "success";
    }

    @RequestMapping("register")
    public String register() {
        return "";
    }


}

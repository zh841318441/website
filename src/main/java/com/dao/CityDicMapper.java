package com.dao;

import com.bean.CityDic;
import com.bean.CityDicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CityDicMapper {
    int countByExample(CityDicExample example);

    int deleteByExample(CityDicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CityDic record);

    int insertSelective(CityDic record);

    List<CityDic> selectByExample(CityDicExample example);

    CityDic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CityDic record, @Param("example") CityDicExample example);

    int updateByExample(@Param("record") CityDic record, @Param("example") CityDicExample example);

    int updateByPrimaryKeySelective(CityDic record);

    int updateByPrimaryKey(CityDic record);
}
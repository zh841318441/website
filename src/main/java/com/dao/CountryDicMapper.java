package com.dao;

import com.bean.CountryDic;
import com.bean.CountryDicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CountryDicMapper {
    int countByExample(CountryDicExample example);

    int deleteByExample(CountryDicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CountryDic record);

    int insertSelective(CountryDic record);

    List<CountryDic> selectByExample(CountryDicExample example);

    CountryDic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CountryDic record, @Param("example") CountryDicExample example);

    int updateByExample(@Param("record") CountryDic record, @Param("example") CountryDicExample example);

    int updateByPrimaryKeySelective(CountryDic record);

    int updateByPrimaryKey(CountryDic record);
}
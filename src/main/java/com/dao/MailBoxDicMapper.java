package com.dao;

import com.bean.MailBoxDic;
import com.bean.MailBoxDicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MailBoxDicMapper {
    int countByExample(MailBoxDicExample example);

    int deleteByExample(MailBoxDicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MailBoxDic record);

    int insertSelective(MailBoxDic record);

    List<MailBoxDic> selectByExample(MailBoxDicExample example);

    MailBoxDic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MailBoxDic record, @Param("example") MailBoxDicExample example);

    int updateByExample(@Param("record") MailBoxDic record, @Param("example") MailBoxDicExample example);

    int updateByPrimaryKeySelective(MailBoxDic record);

    int updateByPrimaryKey(MailBoxDic record);
}
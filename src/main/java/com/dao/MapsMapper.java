package com.dao;

import com.bean.Maps;
import com.bean.MapsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MapsMapper {
    int countByExample(MapsExample example);

    int deleteByExample(MapsExample example);

    int insert(Maps record);

    int insertSelective(Maps record);

    List<Maps> selectByExample(MapsExample example);

    int updateByExampleSelective(@Param("record") Maps record, @Param("example") MapsExample example);

    int updateByExample(@Param("record") Maps record, @Param("example") MapsExample example);
}
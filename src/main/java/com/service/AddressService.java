package com.service;

import com.bean.*;
import com.dao.*;
import com.service.imp.AddressMessage;
import com.util.BackStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 11:17 2019/3/7 0007
 */
@Service
public class AddressService implements AddressMessage {
    @Autowired
    AddressMapper addressMapper;
    @Autowired
    CityDicMapper cityDicMapper;
    @Autowired
    MailBoxDicMapper mailBoxDicMapper;
    @Autowired
    CountryDicMapper countryDicMapper;
    @Override
    public List<CountryDic> selectAllCountry() {
        CountryDicExample countryDicExample=new CountryDicExample();
        countryDicExample.createCriteria();
        List<CountryDic> mapsList = countryDicMapper.selectByExample(countryDicExample);
        return mapsList;
    }

    @Override
    public String saveAddress(Address address) {
        int states = addressMapper.insert(address);
        if (states == 1) {
            return BackStatus.SUCCESS;
        }
        return BackStatus.FAIL;
    }

    @Override
    public List<CityDic> selectAllCityByCountry(int id) {
        CityDicExample cityDicExample = new CityDicExample();
        cityDicExample.createCriteria().andCountryDicIdEqualTo(id);
        List<CityDic> cityDicList =cityDicMapper.selectByExample(cityDicExample);
        return cityDicList;
    }

    @Override
    public Integer selectMailBoxId(String code) {
        MailBoxDicExample mailBoxDicExample=new MailBoxDicExample();
        mailBoxDicExample.createCriteria().andMailboxEqualTo(code);
        List<MailBoxDic> list=mailBoxDicMapper.selectByExample(mailBoxDicExample);
        if(list.size()>0){
            return list.get(0).getId();
        }
        return null;
    }
}

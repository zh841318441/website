package com.service;

import com.bean.Card;
import com.dao.CardMapper;
import com.service.imp.CardMessage;
import com.util.BackStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 22:02 2019/3/7 0007
 */
@Service
public class CardService implements CardMessage{
    @Autowired
    CardMapper cardMapper;
    @Override
    public String saveCard(Card card) {
       int states= cardMapper.insert(card);
       if(states==1){
           return BackStatus.SUCCESS;
       }
        return BackStatus.FAIL;
    }

    @Override
    public Map selectDate() {
        List<Integer> dayList=new ArrayList<>();
        List<Integer> yearList=new ArrayList<>();
        Map map=new HashMap();
        for(int i=1;i<13;i++){
            dayList.add(i);
        }
        for(int i=2019;i<2038;i++){
             yearList.add(i);
        }
        map.put("day",dayList);
        map.put("year",yearList);
        return map;
    }
}

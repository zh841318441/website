package com.service;

import com.bean.User;
import com.bean.UserExample;
import com.dao.UserMapper;
import com.utils.SendMailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class LoginService {
    //    @Autowired
//    private MailHelper mailHelper;
    @Autowired
    private SendMailServiceImpl sendMailService;

    public String template = "您的验证码为%s";

    private String subject = "登陆验证码";
    @Autowired
    private UserMapper userMapper;

    public void login() {

    }

    public void register() {

    }

    public String getCodeTemplate(String code) {
        return String.format(template, code);
    }

    /**
     * 发送邮件
     *
     * @param email
     * @param subject
     * @param template
     */
    public void sendEmail(String email, String subject, String content) {

//        mailHelper.sendEmail(email, subject, template);
        sendMailService.sendHtmlMail(subject, content, email);
    }

    public User login(String username, String password, String code) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andEmailEqualTo(username);
        List<User> users = userMapper.selectByExample(userExample);
        if (CollectionUtils.isEmpty(users)){
            return null;
        }
        User user = users.get(0);
        if(user == null || !password.equals(user.getPasword())){
            return null;
        }

        return user;
    }
    public int insertUser(String username, String password){
        User user=new User();
        user.setEmail(username);
        user.setPasword(password);
        userMapper.insert(user);
        return user.getId();
    }
    public String getSubject() {
        return subject;
    }


}

package com.service.imp;

import com.bean.Address;
import com.bean.CityDic;
import com.bean.CountryDic;
import com.bean.Maps;

import java.util.List;

/**
 * @Author:Mr.zheng
 * @Description:
 * @Date: Created in 11:19 2019/3/7 0007
 */
public interface AddressMessage {

    List<CountryDic> selectAllCountry();

    String saveAddress(Address address);

    List<CityDic> selectAllCityByCountry(int id);

    Integer selectMailBoxId(String code);
}

package com.util;

/**
 * @Author:Mr.zheng
 * @Description: 返回码
 * @Date: Created in 11:32 2019/3/7 0007
 */

public class BackStatus {
    public static final String SUCCESS="200";
    public static final String FAIL="500";

}

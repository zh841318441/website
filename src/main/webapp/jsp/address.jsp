<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="head.jsp" %>
<link rel="stylesheet" href="<%=basePath%>css/normalize.css">
<link rel="stylesheet" href="<%=basePath%>css/common.css">
<link rel="stylesheet" href="<%=basePath%>css/address.css">

<body>
    <form class="main">
        <div>
            <h1>添加新地址</h1>
            <label>国家地区</label> </br>
            <select onchange="selectCityAll()" id="country" name="countryId">
                <c:forEach items="${maps}" var="map">
                <option value="${map.id}">${map.country}</option>
            </c:forEach>
            </select>
            </br>
            <label>姓名</label></br>
            <input type="text" value="" name="name"></br>
            <label>邮政编码</label></br>
            <input class="inline-block" type="text" value="" name="postalPrefix" id="prefix" minlength="3"
                maxlength="3">
            <span style="margin: 0 4px;">-</span>
            <input class="inline-block" type="text" value="" name="postalSuffix" id="suffix" onchange="selectCity();" minlength="3"
                maxlength="3"></br>
            <label>城市</label></br>
            <select type="text" value="" name="cityId" id="city" class="selectCity">
            </select>
            </br>
            <label>地址</label></br>
            <input type="text" value="" name="address"></br>
            <label>公司名称</label></br>
            <input type="text" value="" name="company"></br>
            <label>电话号码</label></br>
            <input type="text" value="" name="tel"></br>
            <button class="add-address" type="button" onclick="saveAddress()">添加地址</button></br>
            <input type="text" value="${id}" name="userId" style="display: none">
        </div>
    </form>
    <script>

        function selectCityAll() {
            var countryId = document.getElementById("country").value;
            var url = '<%=basePath%>address/selectAllCityByCountry/' + countryId
            $.getJSON(url, function (obj) {
                for (var i = 0; i < obj.data.length; i++) {
                    $("#city").append("<option value='" + obj.data[i].mialboxId + "'>" + obj.data[i].city + "</option>");
                }

            })
        }

        function selectCity() {
            var prx = document.getElementById("prefix").value;
            if (prx == null || prx == '') {
                alert("请输入前缀");
            } else {
                var suf = document.getElementById("suffix").value;
                var url = '<%=basePath%>address/selectCityByMailBox/' + prx + suf
                $.getJSON(url, function (obj) {
                    $(".selectCity").val(obj.data);
                })
            }

        }

        function saveAddress() {
            $.ajax({
                url: '<%=basePath%>address/save',
                type: 'post',
                dataType: 'json',
                async: 'false',
                data: $('form').serialize(),
                success: function (data) {
                    alert(data.msg);
                    window.location='<%=basePath%>card/showCard/${id}'
                }
            })
        }
    </script>
</body>

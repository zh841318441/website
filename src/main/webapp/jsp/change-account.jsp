<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="head.jsp" %>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修改账户付款方式</title>
    <link rel="stylesheet" href="<%=basePath%>/css/normalize.css">
    <link rel="stylesheet" href="<%=basePath%>/css/common.css">
    <link rel="stylesheet" href="<%=basePath%>/css/change-account.css">
</head>
<body>
    <div class="main">
        <h1>残高</h1>
        <div class="box">
            <div class="left">
                <div class="card"></div>
                <div class="name">亚马逊礼券</div>
                <div class="aaa">
                    <a >购买礼券</a>
                    <span>|</span>
                    <a>注册礼券</a>
                </div>
            </div>
            <div class="num">
                <span class="icon">¥</span>
                <span class="num-value">0</span>
            </div>
        </div>
        <h1>添加新的付款方式    </h1>
        <div class="hr"></div>

        <h3>信用卡</h3>
        <div>以下信用卡可用</div>
        <div class="add-card">
            <span class="icon">^</span>
            <a>添加卡片</a>
        </div>
        <form class="form">
            <div class="please">请输入卡信息</div>
            <div class="people">
                <label for="people">持卡人</label>
                <input type="text" class="people" id="people" name="nameCard">
            </div>
            <div class="no">
                <label for="no">卡号</label>
                <input type="text" class="no" id="no" name="cardNumber">
            </div>
            <div class="time">
                <label for="time-start">有效期</label>
                <div>
                    <select name="day" id="time-start" class="time-start" >
                        <c:forEach items="${day}" var="day">
                        <option value="${day}">${day}</option>
                        </c:forEach>
                    </select>
                    <select name="year" id="time-end" class="time-end">
                        <c:forEach items="${year}" var="year">
                        <option value="${year}">${year}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="cvv2">
                <label for="cvv2">CVV2</label>
                <input type="text" id="cvv2" name="cvv" class="cvv2">
            </div>
            <button class="submit-card" type="button" onclick="saveCard();">添加信用卡</button>
            <input type="text" name="userId" style="display: none" value="${id}">
        </form>
    </div>
    <script>
        function saveCard() {
            $.ajax({
                url: '<%=basePath%>card/saveCard',
                type: 'post',
                dataType: 'json',
                async: 'false',
                data: $('form').serialize(),
                success: function (data) {
                    alert(data.msg);
                    window.location='<%=basePath%>card/showSend'

                }
            })
        }
    </script>
</body>


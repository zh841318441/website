<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/3/6 0006
  Time: 22:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link rel="stylesheet" href="/css/UI.css">
</head>

<body>
<div class="main">
    <div class="outer-box">
        <h1>登录</h1>
        <form action="/login/login" method="post">
            <div class="control-group">
                <label for="email">邮箱地址或手机号码</label>
                <input type="text" id="email" name="email">
                <span class="error">${paramErrorMsg}</span>
            </div>
            <div class="control-group password">
                <label for="password">
                    <span class="left">密码</span>
                    <a href="./forget.html" class="right">忘记密码</a>
                </label>
                <input type="password" name="password" id="password">

            </div>
            <div class="control-group password">
                <input type="text" name="code"/>
                <input type="button" value="发送验证码" onclick="settime(this);"/>
                <span class="error">${codeErrorMsg}</span>
            </div>
            <span class="error">${loginErrorMsg}</span>
            <button type="submit" class="login">登录</button>
        </form>
        <div class="item">
            <input type="checkbox" name="dontforgetme" id="dontforgetme">
            <label for="dontforgetme">记住登录状态。</label>
            <a class="desc">
                <span class="desc-text">详情</span>
                <span class="desc-icon"></span>
            </a>
        </div>
        <div class="beginer">
            <h5>Amazon的新客户</h5>
        </div>
        <a class="create-account">
            <span class="create-account-inner">创建您的 Amazon 账户</span>
        </a>
        <div class="ok">
            登录即表示您同意网站的<a>使用条件</a>及<a>隐私声明</a>
        </div>
    </div>
</div>

<div class="a-popover a-declarative a-arrow-top" hidden data-action="a-popover-container a-popover-a11y"
     id="a-popover-1" aria-hidden="false"><span tabindex="0" role="dialog"
                                                class="a-popover-start a-popover-a11y-offscreen"
                                                aria-labelledby="a-popover-header-1"></span>
    <div class="a-popover-wrapper">
        <div class="a-popover-header">
            <button data-action="a-popover-close" class="a-button-close  a-declarative"
                    aria-label="Close"><i class="a-icon a-icon-close"></i></button>
            <h4 class="a-popover-header-content" id="a-popover-header-1">关于记住登录状态</h4>
        </div>
        <div class="a-popover-inner" style="height: auto; overflow-y: auto;">
            <div class="a-popover-content" id="a-popover-content-1">
                <p style="margin: 0 0 14px 0;">选择 “记住登录状态” 可减少您需要在此设备上登录的次数。</p>
                <p style="margin: 0;">为了确保您的账户安全，请仅在您的个人设备上使用此选项。</p>
            </div>
        </div>
        <div class="a-arrow-border" style="left: 202.5px;">
            <div class="a-arrow"></div>
        </div>
    </div>
    <span tabindex="0" class="a-popover-end a-popover-a11y-offscreen"></span>
</div>

<script>
    var oldonload = window.onload;

    function OnLoad() {
        var descDom = document.getElementsByClassName('desc');
        var popover = document.getElementsByClassName('a-popover');
        var close = document.getElementsByClassName('a-button-close');
        var p = popover.item(0);
        close.item(0).addEventListener('click', function () {
            p.classList.add('hidden');
            p.style = '';
        })
        descDom[0].addEventListener('click', function () {
            p.classList.remove('hidden');
            var windowwidth = window.innerWidth;
            var width = windowwidth / 2 - p.clientWidth / 2;
            var top = descDom[0].getBoundingClientRect().top - 135;
            popover.item(0).style = 'z-index: 299;visibility: visible;top: ' + top + 'px;left:' + width + 'px;opacity: 1;display: block;'
        })
    }

    if (typeof oldonload === 'function') {
        window.onload = function () {
            oldonload();
            OnLoad();
        }
    } else {
        window.onload = OnLoad;
    }
</script>
<script>
    var counts = 60;

    function settime(val) {
        if (counts == 0) {
            val.removeAttribute("disabled");
            val.value = "获取验证码";
            counts = 60;
            return false;
        } else {
            val.setAttribute("disabled", true);
            val.value = "重新发送（" + counts + ")";
            counts--;
        }
        if (counts === 59) {
            console.log("send");
            var email = document.getElementById("email").value;
            var xmlhttp;
            if (window.XMLHttpRequest) {
                //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
                xmlhttp = new XMLHttpRequest();
            }
            else {
                // IE6, IE5 浏览器执行代码
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
                }
            };
            xmlhttp.open("GET", "/login/sendCode?email=" + email, true);
            xmlhttp.send();
        }
        setTimeout(function () {
            settime(val);
        }, 1000);

    }
</script>
</body>

</html>

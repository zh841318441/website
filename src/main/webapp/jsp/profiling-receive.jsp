<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="head.jsp" %>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profiling Receive</title>
    <link rel="stylesheet" href="<%=basePath%>css/normalize.css">
    <!-- <link rel="stylesheet" href="./css/common.css"> -->
    <link rel="stylesheet" href="<%=basePath%>css/profiling-receive.css">
</head>

<body>
    <div class="main">
        <div class="login"></div>
        <h5>Added Protection</h5>
        <div>DC Web xxxxxxxxxxxxx-xxxx</div>

        <form class="form">
            <div class="group">
                <div class="key">加盟店名:</div>
                <div class="value">YAMADA-DENKI WEB.COM</div>
            </div>
            <div class="group">
                <div class="key">利用金额:</div>
                <div class="value">JPY 149,470</div>
            </div>
            <div class="group">
                <div class="key">利用日:</div>
                <div class="value">19/03/06</div>
            </div>
            <div class="group">
                <div class="key">番号:</div>
                <div class="value">XXXX XXXX XXXX 7391</div>
            </div>
            <div class="group">
                <label class="key" for="id">ID:</label>
                <input type="text" class="value" id="id">
            </div>
            <div class="group">
                <label for="emm" class="key">emm:</label>
                <input type="text" class="value" id="emm">
            </div>
            <div class="group">
                <div class="value">
                    <button class="submit" type="button" >送信</button>
                    <span class="icon">?</span>
                    <a>xxx</a>
                    <a>xxx</a>
                </div>
            </div>
        </form>
        <div>加盟店xxxxxxxxxxxxxxxxxxx</div>
    </div>
    <script>
        $(function () {
            var $submit = $('.submit');
            var $form = $('.form');
            $submit.click(function () {
                var formValues = $form.serializeArray();
                alert("发送成功");
            });
        });
    </script>
</body>

</html>
